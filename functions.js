/**
----------------------------------------------------------------------------------
  DOM ELEMENTS 
----------------------------------------------------------------------------------
*/

//WORK
const workButtonElement = document.getElementById("work-button");
const currentBalanceElement = document.getElementById("work-balance");
const bankButtonElement = document.getElementById("bank-button");
const bankBalanceElement = document.getElementById("bank-balance");
const payElement = document.getElementById("pay");
const repayButtonElement = document.getElementById("repay");

//Laptop
const dropdownElement = document.getElementById("dropdown");
const laptopTitleElement = document.getElementById("laptop-title");
const laptopImageElement = document.getElementById("laptop-image");
const laptopDescriptionElement = document.getElementById("laptop-description");
const laptopPriceElement = document.getElementById("laptop-price");

//Bank
const outstandingLoanElement = document.getElementById("outstanding-loan");
const getLoanButtonElement = document.getElementById("get-loan-button");
const balanceLabelElement = document.getElementById("balance-label");
const buyNowButtonElement = document.getElementById("buy-now-button");
const loanElement = document.getElementById("loan");

/**
----------------------------------------------------------------------------------
  Variables
----------------------------------------------------------------------------------
*/
// Increase with 100;
let currentBalance = 0;
// Bank balance after buy
let bankBalanceAfterBuy = 0;
// Bank balance
let bankBalance = 400;
// initiate balance with 400;
balanceLabelElement.innerText = "€" + bankBalance;
//  One laptop
let laptop = 0;
// loan
let loan = 0;
let outstandingLoan = 0;
let deduction = 0;
// List of laptops
const laptops = [];
/**
----------------------------------------------------------------------------------
  Functions
----------------------------------------------------------------------------------
*/

// Makes a GET request
// Promise
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
  .then((response) => {
    return response.json();
  })
  .then((laptopResponse) => {
    laptops.push(...laptopResponse);
    renderLaptopDropdown(laptops);
  });

// Add laptop to dropdown
function renderLaptopDropdown(laptop) {
  for (const laptop of laptops) {
    dropdownElement.innerHTML += `<option value=${laptop.id}>${laptop.title}</option>`;
  }
}

//Add images to laptop
function renderSelectedLaptop(laptop) {
  laptopTitleElement.innerText = "Title: " + laptop.title;
  laptopDescriptionElement.innerText = "Description: " + laptop.description;
  laptopPriceElement.innerText = "€" + Number(laptop.price);
  laptopImageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" + laptop.image;
}

// Select laptop
function onSelectChange() {
  const laptopId = Number(this.value);
  laptop = laptops.find((laptop) => laptop.id === laptopId);
  renderSelectedLaptop(laptop);
  console.log(laptopId);
}

// Increase balance by 100
function increasePayBalance() {
  currentBalance += 100;
  currentBalanceElement.innerText =
    "Pay balance " + "€" + Number(currentBalance);
}

// Transfer the money from work balance to bank balance
function transferMoney() {
  if (loan > 0) {
    outstandingLoan = currentBalance * 0.1;

    loan = loan - outstandingLoan;
    bankBalance = bankBalance + (currentBalance - outstandingLoan);
  }
  bankBalance += Number(currentBalance);
  bankBalanceElement.innerText = "Bank Balance " + "€" + bankBalance;
  currentBalance = 0;
  currentBalanceElement.innerText = currentBalance;
  loanElement.innerText = "Loan: €" + loan;
  bankBalanceDisplay();
}
function showPay() {
  payElement.innerText = "€" + Number(currentBalance);
  alert(payElement.innerText);
}

// Outstanding loan display
function bankBalanceDisplay() {
  balanceLabelElement.innerText = "€" + bankBalance;
}
// Get a Loan from the bank
function getLoan() {
  // Popup box to get input
  if (Number(loan) > 0) {
    alert("Yo can not loan money");
  } else {
    let doubleBankBalance = bankBalance + bankBalance;
    inputLoan = prompt("Please enter the amount");
    console.log(doubleBankBalance);
    if (Number(inputLoan) > doubleBankBalance) {
      alert("Yo can not loan money");
    } else {
      loan = inputLoan;
      loanElement.innerText = "Loan: €" + loan;
      repayButtonElement.style.display = "inline";
      balanceLabelElement.innerText = bankBalance + Number(loan);
    }
  }
}

// Check of the bank balance is enough
function checkBankBalance() {
  if (bankBalance < Number(laptop.price)) {
    alert("You cannot afford the laptop");
  } else {
    alert("You are now the owner of the laptop");
    bankBalanceAfterBuy = bankBalance - Number(laptop.price);
    balanceLabelElement.innerText = bankBalanceAfterBuy;
  }
}

//Buy function
function buyNow() {
  checkBankBalance();
  bankBalance = 0;
  bankBalanceElement.innerText = Number(currentBalance);
}

// Repay loan
function repayLoan() {
  let repay = currentBalance - loan;
  balanceLabelElement.innerText = bankBalance + currentBalance;
  loan = loan - repay;
  loanElement.innerText = "Loan: €" + repay;
}

/**
----------------------------------------------------------------------------------
  Events
----------------------------------------------------------------------------------
*/
// Repay loan
repayButtonElement.addEventListener("click", repayLoan);
// Get loan
getLoanButtonElement.addEventListener("click", getLoan);
// Increase pay balance
workButtonElement.addEventListener("click", increasePayBalance);
// Transfer money
bankButtonElement.addEventListener("click", transferMoney);
// Change by selecting a laptop
dropdownElement.addEventListener("change", onSelectChange);
// Buy now
buyNowButtonElement.addEventListener("click", buyNow);
// Image error handle
laptopImageElement.addEventListener("error", () => {
  laptopImageElement.src = "image-not-found.jpg";
});
